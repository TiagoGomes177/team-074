# Report - Model Validation With Epsilon

## Ponto 1 - Describe the concern “Model Validation” 

O Model Validation é uma forma de demonstrar que um dado modelo é uma representação aceitável de um dado Sistema: que reproduz o comportamento do Sistema com fidelidade suficiente para satisfazer os seus objetivos. 

Como um modelo é normalmente construído para analisar um problema particular, pode representar diferentes partes de um Sistema a diferentes níveis de abstração. Como tal, o modelo pode conter diferentes níveis de validação para diferentes partes do mesmo. 

Para a grande maioria dos modelos, existe três aspetos diferentes que devem ser considerados durante a validação de um modelo: 

-	Suposições (assumptions) 

-	Inputs de valores de parâmetros (input parameters values) 

-	Valores de output (output values) 

No entanto, em prática pode ser difícil atingir tal nível de validação, especialmente se o Sistema modelado não existir ainda. Em geral, uma validação inicial concentra-se no output do modelo, e caso a validação sugira algum problema será realizada uma validação mais detalhada. 

Amplamente falando, existem três abordagens para a validação de um modelo: 

-	Intuição de especialista (Expert Intuition) 

-	Medições de um Sistema real (Real System Measurements) 

-	Resultados/Analises teóricas (Theoretical results/analysis)

## Ponto 2 - Describe the approach "Model Validation With Epsilon"

EVL (Epsilon Validation Language) é uma linguagem de validação de modelos baseada em EOL (Epsilon Object Language) com o objetivo de conferir capacidades de validação de modelos ao Epsilon. 
Esta linguagem estende, de forma conceptual, as capacidades do OCL (Object Constraint Language), sendo que alem das funcionalidades de avaliação de restrições em modelos e tecnologias de modelação que tem inerentemente do OCL, também possui funcionalidades como dependências entre restrições, mensagens de erro personalizadas, especificação de correções e avaliação de restrições entre modelos.

## Ponto 3 - Present a simple case study illustrating how to apply the approach:

Elemento tem um nome:

-	Criamos uma constraint “HasName” que verifica sempre se o nome é diferente de “” e caso este seja vazio emite uma mensagem. 

-	Uma constraint é algo que captura erros críticos que invalidam um modelo.

Feature (atributo, referência, parâmetro) tem nome e começa por letra minúscula:

-	Neste caso é utilizado uma critique em vez de uma constraint. 

-	Uma critique é utilizada quando se pretende capturar situações que não são criticas para o modelo, isto é não o invalida, mas que no entanto devem ser verificadas para aumentar a qualidade do modelo.

-	Nesta verificação, verificamos se a feature satisfaz a constraint “HasName”, sendo que depois s verifica então se o primeiro caracter do nome começa com letra minúscula.

Classe tem nome e começa por letra maiúscula:

-	Neste caso, tal como anteriormente, a verificação do caracter não é algo que invalida o modelo, mas sim que aumenta a qualidade do mesmo. Como tal usamos novamente uma critique.

-	Segue o mesmo formatado do caso anterior, no entanto caso a verificação retorne falso, emite uma mensagem e é utilizado um “fix” que permite dar classes, usando uma “ExpressionOrStatementBlock” em vez de um String estática. 

-	A parte do “do” é onde acontece propriamente a alteração. 

Classe não se herda a si mesma diretamente ou indiretamente:

-	Como esta verificação afeta diretamente a validade do modelo, é necessário utilizar uma constraint.

Notas:

-	Context: A context specifies the kind of instances on which the contained invariants will be evaluated. Each context can optionally define a guard which limits its applicability to a narrower subset of instances of its specified type. Thus, if the guard fails for a specific instance of the type, none of its contained invariants are evaluated. 

-	Guard: Guards are used to limit the applicability of invariants (Section 4.1.4). This canbe achieved at two levels. At the Contextlevel it limits the applicability of all invariants of the context and at the Invariant level it limits the applicability of a specific invariant. 

-	Fix: A fix defines a title using an ExpressionOrStatementBlock instead of a static String to allow users to specify context-aware titles (e.g. Rename class customer to Customer instead of a generic Convert first letter to upper-case). Moreover, the do part is a statement block where the fixing functionality can be defined using EOL. The developer is responsible for ensuring that the actions contained in the fix actually repair the identified inconsistency. 

-	Constraint: Constraints in EVL are used to capture critical errors that invalidate the model. As discussed above, Constraint is a sub-class of Invariant and therefore inherits all its features. 

-	Critique: Unlike Constraints, Critiques are used to capture non-critical situations that do not invalidate the model, but should nevertheless be addressed by the user to enhance the quality of the model. 

## Ponto 4 - Compare the approach with the approaches lectured in the EDOM course:

A principal motivação para a criação do EVL é tentar de forma conceptual estender o OCL, para tentar lidar com as limitações causadas por este ser puramente declarativo e evitar ter quaisquer “efeitos secundários”. Sabendo isto também sabemos que o EVL inerentemente tem todas as funcionalidades do OCL, sendo que esta comparação se foca nas limitações que o EVL consegue ultrapassar.

-	Feedback de utilizador limitado: No caso de uma invariante falhar no OCL este encontra-se limitado apenas conseguindo notificar o utilizador da invariante que falhou e em que instância. Para ultrapassar esta limitação o EVL permite a definição de uma mensagem sob a forma de um ExpressionOrStatementBlock que devolve uma string com a descrição de porque é que a invariante falhou.

-	Inexistência de suporte para avisos: Ambientes de desenvolvimento mais contemporâneos quando verificam a consistência e coerência de artefactos costumam produzir dois tipos distintos de feedback, erros e avisos. Erros indicam que existe um problema critico que invalida o artefacto enquanto um aviso indica um problema não critico que embora deva ser tratado na mesma. No OCL não existe a distinção entre erros e avisos sendo estes considerados também erros dificultando assim a perceção e priorização da resolução de problemas. Para ultrapassar esta limitação no EVL as invariantes são classes abstratas que são utilizadas como superclasses para os tipos específicos de erros e avisos.

-	Inexistência de suporte para dependência de restrições: Tal como o nome indica o OCL não suporta dependência de restrições sendo que cada invariante é completamente isolada de todas as outras. Isto por vezes pode criar complicações adicionais e desnecessárias. Por exemplo, imaginemos uma situação em que temos duas invariantes em que, respetivamente, o nome de uma classe não pode ser vazio e o nome de uma classe tem de começar por uma letra maiúscula. Neste caso se a primeira invariante falhar não só seria irrelevante verificar a segunda invariante como seria problemático, pois seria necessário despender de recursos para a verificar e iria criar um erro separado que só iria gerar confusão. Para ultrapassar esta limitação o EVL permite a definição de dependências entre invariantes através de comandos como satisfies(), satisfiesAll(), satisfiesOne(), etc.

-	Inexistência de suporte para a reparação de inconsistências: Embora o OCL consiga detetar inconsistências nos artefactos este não as consegue reparar por mais simples que a inconsistência possa ser. Isto deve-se a ser uma linguagem sem “efeitos secundários” não permitindo a modificação de modelos. Para ultrapassar esta limitação o EVL permite reparações semiautomáticas através da definição de reparações nas respetivas invariantes.

-	Inexistência de suporte para restrições inter-modelos: Como o nome indica o OCL apenas avalia um modelo. Esta limitação pode ser bastante severa em casos de desenvolvimento de modelos em grande escala que envolvam modelos distintos. Devido ao EVL ser baseado em EOL este consegue inerentemente aceder a múltiplos modelos de diversos meta-modelos e tecnologias.

## Ponto 5 - Provide a small review of literature references about the approach: 

No que toca a referencias literárias sobre a aproximação em estudo a que foi mais utilizada e que é considerada mais útil e completa é o livro “The Epsilon Book” dos autores Dimitris Kolovos, Louis Rose, Antonio García-Domínguez e Richard Paige. Mais especificamente o capítulo 4 deste livro, sendo este em grande parte baseado no artigo científico feito por Dimitris Kolovos, Richard Paige e Fiona Polack (sendo alguns destes tambem autores do livro previamente mencionado), “On the Evolution of OCL for Capturing Structural Constraints in Modelling Languages”.

- Model Validation and Verification. (n.d.). Retrieved December 2, 2018, from http://www.inf.ed.ac.uk/teaching/courses/ms/notes/note14.pdf 

- Example: Validate an OO model with EVL. (n.d.). Retrieved December 2, 2018, from https://www.eclipse.org/epsilon/examples/index.php?example=org.eclipse.epsilon.examples.validateoo

- Kolovos, D., Rose, L., García-Domínguez, A., & Paige, R. (n.d.). The Epsilon Book. Retrieved November 26, 2018, from https://www.eclipse.org/epsilon/doc/book/

- Kolovos, D., Paige, R., & Polack, F. (n.d.). On the evolution of OCL for capturing structural constraints in modelling languages. Retrieved November 26, 2018, from https://www.researchgate.net/publication/221350095_On_the_Evolution_of_OCL_for_Capturing_Structural_Constraints_in_Modelling_Languages 